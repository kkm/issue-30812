Wiki pages here demonstrate either the problem in the issue
[gitlab-org/gitlab-ce#30812](https://gitlab.com/gitlab-org/gitlab-ce/issues/30812)
or, hopefully, the fix to the above.